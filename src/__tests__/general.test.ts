import { ifThen, megaBoolean } from '../general'

describe('general utils', () => {
    describe('ifThen', () => {
        it('should call the then function provided if condition evaluates to true', () => {
            const mockThen = jest.fn()
            ifThen(true, mockThen)
            expect(mockThen).toHaveBeenCalled()
        })
        it('should not call the then function provided if condition evaluates to false', () => {
            const mockThen = jest.fn()
            ifThen(false, mockThen)
            expect(mockThen).not.toHaveBeenCalled()
        })
        it('should call the else function if one is provided and the condition evaluates to false', () => {
            const mockThen = jest.fn()
            const mockElse = jest.fn()
            ifThen(false, mockThen, mockElse)
            expect(mockThen).not.toHaveBeenCalled()
            expect(mockElse).toHaveBeenCalled()
        })
    })

    describe('megaBoolean', () => {
        it('should return false for all normally falsy values', () => {
            const allFalsies = [null, undefined, 0, '', NaN, false]
            allFalsies.forEach(falsy => {
                expect(megaBoolean(falsy)).toBe(false)
            })
        })
        it('should return false for an empty object, an empty array, an empty map, or an empty set', () => {
            ;[[], {}, new Map(), new Set()].forEach(item => {
                expect(megaBoolean(item)).toBe(false)
            })
        })

        it('should return true for all other values', () => {
            ;[
                { a: 'non-empty object' },
                ['non-empty array'],
                1,
                'hi',
                new Map([['a', 'b']]),
                new Set([1, 2, 3]),
            ].forEach(item => {
                expect(megaBoolean(item)).toBe(true)
            })
        })
    })
})
