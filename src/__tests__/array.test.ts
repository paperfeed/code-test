import {
    arrayOf,
    isIn,
} from '../array'

describe('Array utils', () => {
    describe('function arrayOf', () => {
        it('should generate an array with amount of items deducted from the parameter', () => {
            const amount = 5
            const array = arrayOf(amount)
            const expectedArray: typeof array = [0, 1, 2, 3, 4]

            expect(array).toEqual(expectedArray)
        })

        it('should generate an empty array if the amount specified is zero', () => {
            const array = arrayOf(0)
            const expectedArray: typeof array = []

            expect(array).toEqual(expectedArray)
        })

    })

    describe('isIn', () => {
        it('should return a function', () => {
            expect(typeof isIn([1, 2, 3, 4])).toBe('function')
        })
        it('should return a function which returns a Boolean', () => {
            expect(typeof isIn([1, 2, 3, 4])(3)).toBe('boolean')
        })
        it('should return a function which returns true if passed an item present in the array isIn is called with', () => {
            const check = isIn([1, 2, 3, 4])
            expect(check(3)).toBe(true)
        })
        it('should return a function which returns false if passed an item not present in the array isIn is called with', () => {
            const check = isIn([1, 2, 3, 4])
            expect(check(5)).toBe(false)
        })
        it('should be useful in .filter, .some, and .every', () => {
            const array = [1, 2, 3, 4, 5]
            const otherArray = [5, 6, 7, 8, 9]
            expect(array.some(isIn(otherArray))).toBe(true)
            expect(array.filter(isIn(otherArray))).toEqual([5])
            expect(array.every(isIn(otherArray))).toBe(false)
        })
    })
})
