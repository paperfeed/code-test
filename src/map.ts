export function findValueInMap<K, V>(map: Map<K, V>, testFn: (value: V) => boolean): V | undefined {
    for (const value of map.values()) {
        if (testFn(value)) return value;
    }
}


export function findKeyInMap<K, V>(map: Map<K, V>, testFn: (value: V) => boolean): K | undefined {
    for (const [key, value] of map.entries()) {
        if (testFn(value)) return key;
    }
}


export function findKeyValuePairInMap<K, V>(map: Map<K, V>, testFn: (value: V) => boolean): {key: K | undefined, value: V | undefined} {
    for (const [key, value] of map.entries()) {
        if (testFn(value)) return {key, value};
    }
    return { key: undefined, value: undefined};
}
