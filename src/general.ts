export function ifThen(condition: boolean, trueFunction: () => unknown, elseFunction?: () => unknown) {
    if (condition) trueFunction()
    else if (elseFunction) elseFunction()
}


export function megaBoolean(test: any): boolean {
    return !!(test && (test > 0 || test.size || test.length || Object.keys(test).length))
}
