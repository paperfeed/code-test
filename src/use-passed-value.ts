export function usePassedValue<ValidProp>(prop: string, transformFn?: (value: any) => string) {

    // Didn't figure out exactly what this function is supposed to do based from the tests
    // ...But it works
    return {
        [prop]: (props: ValidProp) => {
            if (prop in props) {
                const value = (props as any)[prop]

                return {
                    [prop]: transformFn ? transformFn(value) : value
                }
            }
        }
    } ;
}
