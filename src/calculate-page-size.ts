export function calculatePageSize(totalItems: number, currentPage: number, itemsPerPage: number) {
    const onLastPage = currentPage === Math.floor(totalItems / itemsPerPage);

    if (onLastPage) {
        return totalItems - currentPage * itemsPerPage
    }

    return itemsPerPage;
}
