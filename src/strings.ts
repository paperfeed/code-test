const replaceCharAt = (string: string, index: number, newChar: string) => {
    return string.substring(0, index) + newChar + string.substring(index + 1)
}


export function capitalizeFirstLetter(string: string): string {
    string = replaceCharAt(string, 0, string.charAt(0).toUpperCase())
    return string
}


export function capitalizeEachWord(string: string): string {
    return string.split(' ').map(element => capitalizeFirstLetter(element)).join(' ')
}


export function spacesToUnderscores(string: string): string {
    return string.replace(/ /g, '_')
}


export function toSnakeCase(string: string): string {
    // Filter is to filter out empty elements (due to how split works)
    return string.split(/([A-Z][a-z]+)/).filter(x => x !== '').join('_').toLowerCase()
}
