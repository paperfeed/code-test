export function arrayOf(amount: number): Array<number> {
    if (amount === 0) return []

    const result = new Array(amount).fill(amount - 1);
    // Array reduced backwards to sort it from low to high
    result.reduce((previousValue, currentValue, index) => result[(amount - 1) - index] = previousValue - 1)

    return result
}


export function isIn<T>(testArray: Array<T>): (test: T) => boolean {
    return (test) => {
        return testArray.some(element => element === test)
    }
}
